const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
function keys(obj) {
    if(!obj)return;
    const keys=[]
    for(let key in obj){
        keys.push(key)
    }
    return keys
}
console.log(keys(testObject))