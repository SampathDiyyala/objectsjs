const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

function invert(obj) {
    if(!obj)return;
    const keys=[]
    const values=[]
    for(let key in obj){
        keys.push(key)
        values.push(obj[key])
    }
    var invertarr={}
    for(let i=0;i<keys.length;i++){
        invertarr[values[i]] = keys[i]
    }
    return invertarr
}
console.log(invert(testObject))